import socket
HOST = '127.0.0.1'
PORT = 51515

# create a socket object
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print('The socket was declared '+str(s))


# connection to hostname on the port.
s.connect((HOST, PORT))
print('The socket was connected '+str(s))


# Receive no more than 1024 bytes
msg = s.recv(1024)

s.close()

print (msg.decode('ascii'))