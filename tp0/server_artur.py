
# coding: utf-8

# In[ ]:

# Student: Artur Henrique Fonseca dos Santos
# Number: 2013007137
# 
# TP0 - Counter - Server side

import socket
from struct import *

# Initialize global variables and constants
counter = 0
host = '127.0.0.1'
port = 51515

# Initialize server connection on host:port
def initialize_connection(host, port, mode):
    socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    
    if (mode == 'passive'):
        socket.bind((host, port))
        socket.listen(1)
    elif (mode == 'active'):
        socket.connect((host, port))
    
    return socket

# Send 'data' through the 'socket' of 'size' bytes
# If the data isn't yet formatted, set 'to_pack' and pass the right format string 'fmt'
# Raises an exception if the connection was lost
def send_data(socket, data, size, to_pack = False, fmt = '!i'):
    total_sent = 0
    # pack data to specified format
    if(to_pack):
        to_send = pack(fmt, data)
    else:
        to_send = data
        
    # make sure all the data was sent, even if sent in different packages
    while(total_sent < size):
        sent = socket.send(to_send[total_sent:])
        if(sent == 0):
            raise RuntimeError('Connection to client lost')
        
        total_sent += sent

# Receive 'data' through the 'socket' of 'size' bytes
# If the package needs to be reformatted, set 'to_unpack' and pass the right format string 'fmt'
# Raises an exception if the connection was lost
def receive_data(socket, size = 1, to_unpack = False, unpack_fmt = ''):
    total_received = 0
    data = ''
    
    # make sure all the data was received, even if sent in different packages
    while(total_received < size):
        socket.settimeout(1)
        chunk = socket.recv(size - total_received)
        if(chunk == ''):
            raise RuntimeError('Connection to client lost')

        data += chunk
        total_received += len(chunk)
    
    # unpack data from network format to specified format
    if(to_unpack):
        data = unpack(unpack_fmt, data)
        if(len(data) == 1):
            data = data[0]
    
    return data

server_socket = initialize_connection(host, port)

# Server loop
while 1:
    client_socket, client_address = server_socket.accept()
    try:
        op = receive_data(client_socket, 1)

        # Store temporary counter
        if(op == '+'):
            temp_counter = counter + 1
            if(temp_counter == 1000):
                temp_counter = 0
        elif(op == '-'):
            temp_counter = counter - 1
            if(temp_counter == -1):
                temp_counter = 999
        else:
            continue

        send_data(client_socket, temp_counter, 4, True, '!i')
        response = receive_data(client_socket, 3)

        # close connection
        client_socket.close()

        # Update counter if response is correct
        if(str(temp_counter).zfill(3) == response):
            counter = temp_counter
            print counter
            
    # Treat closed connection
    except RuntimeError as e:
        continue
        
    # Treat timeout
    except socket.timeout as e:
        print "T"
        client_socket.close()
        continue

    # Treat forced interruption
    except KeyboardInterrupt as e:
        client_socket.close()
        server_socket.close()
        break
