"""
Aluna: LAURA VIANNA LUCCIOLA - 2013007544
Disciplina: DCC023 - Redes de Computadores
Trabalho Pratico 0
"""

""" IMPORTING NECESSARY MODULES"""
# imports sockets module, which are endpoints of bidirectional communication channel
import socket
# imports struct module, needed for packing info in network binary
import struct
""" IMPORTING NECESSARY MODULES"""


""" SETTING UP THE GLOBAL VARIABLES"""
# initializing the global counter as zero
GLOBAL_COUNTER = 0
# setting host, port and timeout according to specification
HOST = '127.0.0.1'
PORT = 51515
TIMEOUT_SECS = 1
""" SETTING UP THE GLOBAL VARIABLES"""


# receives a socket "s" and closes it's connection if the socket exists
def close_connection(s):
    if s is not None:
        s.close()


# print flagged debug messages if global var. DEBUG is true
def debug(message):
    if DEBUG:
        print('[debug] ' + message)

DEBUG = False

# socket domain ("family of protocols that is used as the transport mechanism"): INET, UNIX...
# socket type (of communication): STREAM, DGRAM
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client_socket = None
debug('The server_socket was declared ' + str(server_socket))
debug('A blank client_socket was declared ' + str(client_socket))

# binds address to a socket ((hostname, port number)-> pair)
server_socket.bind((HOST, PORT))
debug('The server_socket was bond ' + str(server_socket))

# queues up one TCP request at a time
server_socket.listen(1)
debug('The server_socket is listening')

while True:
    try:
        # establish a connection with client and set timeout to 1 second
        (client_socket, address) = server_socket.accept()
        server_socket.settimeout(TIMEOUT_SECS)
        debug('The server accepted a client_socket' + str(client_socket) + ' on address ' + str(address))

        # receives + or - and operates over a temporary counter
        data = client_socket.recv(1).decode('ascii')
        debug('The server received ' + data)

        if data == '+':
            temp_counter = 0 if GLOBAL_COUNTER == 999 else GLOBAL_COUNTER + 1
        elif data == '-':
            temp_counter = 999 if GLOBAL_COUNTER == 0 else GLOBAL_COUNTER - 1
        else:
            # terminates client connection when invalid input is read from client
            close_connection(client_socket)
            debug('The client socket connection are closed')


        debug('The GLOBAL_COUNTER is now ' + str(GLOBAL_COUNTER))
        debug('The temp_counter is now ' + str(temp_counter))

        # packs temp_counter into network binary code (big-endian) and sends to client as int
        binary_data = struct.pack("!I", temp_counter)
        client_socket.send(binary_data)

        # receives response from client
        response = client_socket.recv(3)
        debug('The client_socket response was ' + str(response))

        # updates the global counter if response is the same as temp_counter and closes connection
        if int(response.decode('ascii')) == temp_counter:
            GLOBAL_COUNTER = temp_counter
            print(str(GLOBAL_COUNTER))

        debug('The GLOBAL_COUNTER is now ' + str(GLOBAL_COUNTER))

        close_connection(client_socket)

        debug('The client_socket connection is closed')

    # closes client connection and continues execution when bad decode
    except ValueError:
        close_connection(client_socket)
        debug('Bad response ASCII decode')


    # terminates connection CTRL+C is typed
    except KeyboardInterrupt:
        close_connection(client_socket)
        close_connection(server_socket)
        debug('The server and client socket connection are closed')
        break

    # when the connection times out, closes connection on server and client
    except socket.timeout:
        close_connection(client_socket)
        print('T')


# sources:
# https://www.tutorialspoint.com/python3/python_networking.htm
# https://docs.python.org/3/library/struct.html#byte-order-size-and-alignment
