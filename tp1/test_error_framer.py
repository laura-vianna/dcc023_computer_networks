# TODO: turn off debugger
DEBUG = True

# coding: utf-8
# Student: Artur Henrique Fonseca dos Santos- 2013007137
# Student: Laura Vianna Lucciola - 2013007544
# TP01 - Data framing

import sys
import threading
import socket
import struct

sync = bytearray.fromhex('DCC023C2')

# Prints debug messages if DEBUG is set
def debug(message):
    if DEBUG:
        print('[debug] ' + message)

# -----Given Chechsum algorithm ----- #
def carry_around_add(a, b):
    c = a + b
    return(c &0xffff)+(c >>16)

def checksum(msg):
    s = 0
    for i in range(0, len(msg),2):
        w = msg[i]+((msg[i+1])<<8)
        s = carry_around_add(s, w)
    return~s &0xffff
# -----Given Chechsum algorithm ----- #

# Initialize server connection on host:port, according to given mode
def initialize_connection(host, port, mode):
    framer_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    if (mode == 'passivo'):
        framer_socket.bind((host, port))
        framer_socket.listen(1)
        client_socket, client_address = framer_socket.accept();
        return client_socket

    elif (mode == 'ativo'):
        framer_socket.connect((host, port))
        return framer_socket

# Sends 'data' through the 'framer_socket' of 'size' bytes
# Raises an exception if the connection was lost
def send_data(framer_socket, data, size):
    total_sent = 0

    # make sure all the data was sent, even if sent in different packages
    while(total_sent < size):
        sent = framer_socket.send(data[total_sent:])
        if(sent == 0):
            raise RuntimeError('Connection to client lost')

        total_sent += sent

# Receive 'data' through the 'framer_socket' of 'size' bytes
# If the package needs to be reformatted, set 'to_unpack' and pass the right format string 'fmt'
# Raises an exception if the connection was lost
def receive_data(framer_socket, size = 1, to_unpack = False, unpack_fmt = ''):
    total_received = 0
    data = bytearray()

    # make sure all the data was received, even if sent in different packages
    while(total_received < size):
        chunk = framer_socket.recv(size - total_received)
        if(chunk == ''):
            raise RuntimeError('Connection to client lost')

        data += chunk
        total_received += len(chunk)

    # unpack data from network format to specified format
    if(to_unpack):
        data = struct.unpack(unpack_fmt, data)
        if(len(data) == 1):
            data = data[0]

    return data

# Reads all data, as bytes, via 'framer_socket'.
# Decodes the frame, handling the sync and assync states and checksum calculation
# Writes a sucessful result in the output file 'file_name'
def receive_file(framer_socket, file_name):
    with open(file_name, "wb") as output_file:
        in_sync = False

        correct_pack = 0
        sync_is_lost = 0
        while 1:
            debug('Receive file method')
            try:
                bytes_read = 0
                sync_read = False
                data = bytearray()

                # Read sync bytes when in syncronized state
                if((not sync_read) and in_sync):
                    data = receive_data(framer_socket, 4)
                    if(data == sync):
                        data += receive_data(framer_socket, 4)
                        if(data == sync+sync):
                            sync_read = True
                            sync_data = data
                        else:
                            in_sync = False
                            data = data[4:]
                            bytes_read = len(data)
                    else:
                        in_sync = False
                        bytes_read = len(data)

                # Recover syncrony by reading each single byte sent searching for two sync bytes
                while(not in_sync):
                    if(bytes_read < 4):
                        data = receive_data(framer_socket, 4 - bytes_read)
                        bytes_read = len(data)
                    else:
                        data = data[1:] + receive_data(framer_socket, 1)

                    if(bytes_read == 4 and data == sync):
                        data += receive_data(framer_socket, 4)

                        if(data == sync+sync):
                            in_sync = True
                            sync_read = True
                            sync_data = data
                        else:
                            data = data[4:]

                # Read each part of the package, verify length and checksum, and if correct, save on output file
                if(sync_read):
                    checksum_data = receive_data(framer_socket, 2)
                    raw_length = receive_data(framer_socket, 2)
                    length = struct.unpack('!H', raw_length)[0]
                    reserved = receive_data(framer_socket, 2)
                    payload = receive_data(framer_socket, length)
                    msg = sync_data+checksum_data+raw_length+reserved+payload
                    if len(msg)%2 != 0:
                        debug('Message has a odd number of bytes -> an all 0 byte was added')
                        chks = checksum(msg+bytearray(1))
                    else:
                        chks = checksum(msg)

                    if(length == 0 or checksum(msg) != 0):
                        in_sync = False
                        sync_read = False
                        debug("Syncrony lost")
                        sync_is_lost += 1

                    else:
                        output_file.write(payload)
                        output_file.flush()
                        debug("Received package of size " + str(len(msg)) + "B")
                        correct_pack += 1


                debug("Received " + str(correct_pack) + " correct packages")
                debug("Received " + str(sync_is_lost) + " corrupted packages")

            except RuntimeError as e:
                continue

# Reads all data from the input file 'file_name', padding an odd number of bytes
# returns a bytearray 'data' and its 'size'
def get_data_from_file(file_name):
    size = 0
    data = bytearray(0)
    bytes_read = bytearray(0)
    with open(file_name, "rb") as input_file:
        bytes_read = input_file.read(1)
        while bytes_read != b'':
            # debug('The bytes_read was '+str(bytes_read))
            data += bytes_read
            size += 1
            bytes_read = input_file.read(1)

    return data, size

# From the given byte array 'data' and its 'size', creates a formatted frame
# Inserts SYNC and reserved sequences and calculates the Check Sum
def build_message_from_data(data, size):
    chks = 0
    rsvd = bytearray(2)

    msg = sync + sync + struct.pack("@H",chks) + struct.pack("!H", size) + rsvd + data
    # debug('The message is: '+str(msg))
    if len(msg)%2 != 0:
        debug('Message has a odd number of bytes -> an all 0 byte was added')
        chks = checksum(msg+bytearray(1))
    else:
        chks = checksum(msg)
    debug("Calculated Checksum: 0x%04x" % chks)
    msg = sync + sync + struct.pack("@H",chks) + struct.pack("!H", size) + rsvd + data
    if len(msg)%2 != 0:
        debug("Check Sum operation gives: 0x%04x" % checksum(msg+bytearray(1)))
    else:
        debug("Check Sum operation gives: 0x%04x" % checksum(msg))

    return msg

def build_message_wrong_checksum(data, size):
    chks = 2
    rsvd = bytearray(2)

    msg = sync + sync + struct.pack("@H",chks) + struct.pack("!H", size) + rsvd + data
    # debug('The message is: '+str(msg))
    if len(msg)%2 != 0:
        debug('Message has a odd number of bytes -> an all 0 byte was added')
        chks = checksum(msg+bytearray(1))
    else:
        chks = checksum(msg)
    debug("Calculated Checksum: 0x%04x" % chks)
    msg = sync + sync + struct.pack("@H",chks) + struct.pack("!H", size) + rsvd + data
    if len(msg)%2 != 0:
        debug("Check Sum operation gives: 0x%04x" % checksum(msg+bytearray(1)))
    else:
        debug("Check Sum operation gives: 0x%04x" % checksum(msg))

    return msg


# Reads all data, as bytes, from the input file 'file_name',
# packs it into the given frame format and sends it via 'framer_socket'
def send_file(framer_socket, file_name):
    # IPv4 data maximum size is 65535 bytes
    MAX_SIZE = 65534

    data, size = get_data_from_file(file_name)
    debug('The data has size '+str(size)+'B')
    # debug('The data is: '+str(data))

    if size < MAX_SIZE:
        msg = build_message_from_data(data, size)
        # debug("Final message, with calcutated checksum is "+str(msg))
        send_data(framer_socket, msg, len(msg))
        debug("Sending package of size " + str(len(msg)) + "B")
    else:
        packages_num = 0
        corrupted_packages_num = 0
        correct_packages_num = 0
        while size > 0:
            debug('The data size '+str(size)+' is bigger then the max '+str(MAX_SIZE))
            data_to_send, temp_data = data[:min(MAX_SIZE, size)], data[min(MAX_SIZE, size):]
            size -= len(data_to_send)
            data = temp_data
            # Each 10 packages, 9 will be correct and one corrupted
            if packages_num % 5 == 0:
                # TODO: choose one
                # If the length is bigger then the real data, the corrupt package and the next correct package will be discarded
                # msg = build_message_from_data(data_to_send, len(data_to_send)+2)
                # If the length is smaller then the real data, will be discarded
                # msg = build_message_from_data(data_to_send, len(data_to_send)-2)
                # If checksum is calculated in wrong way
                msg = build_message_wrong_checksum(data_to_send, len(data_to_send))
                debug("Sending corrupt package of size " + str(len(msg)) + "B")
                corrupted_packages_num += 1

            else:
                msg = build_message_from_data(data_to_send, len(data_to_send))
                debug("Sending package of size " + str(len(msg)) + "B")
                correct_packages_num += 1

            send_data(framer_socket, msg, len(msg))
            packages_num += 1

        debug("Sent " + str(packages_num) + " packages")
        debug("Sent " + str(corrupted_packages_num) + " corrupted packages")
        debug("Sent " + str(correct_packages_num) + " correct packages")


try:
    if len(sys.argv) != 6:
        debug('Wrong number of arguments! Must be "program <in> <out> <ip> <port> <mode>"')
        sys.exit(0)

    in_file = sys.argv[1]
    out_file = sys.argv[2]
    host = sys.argv[3]
    port = int(sys.argv[4])
    mode = sys.argv[5]

    if (mode != 'passivo') & (mode != 'ativo'):
        debug('Unknown mode was given! Must be "ativo" ou "passivo"')
        sys.exit(0)

    framer_socket = initialize_connection(host, port, mode)

    # Create two threads as follows
    send_t = threading.Thread(target = send_file, args = (framer_socket, in_file, ))
    receive_t = threading.Thread(target = receive_file, args = (framer_socket, out_file, ))
    send_t.start()
    receive_t.start()

# terminates connection CTRL+C is typed
except KeyboardInterrupt as e:
    framer_socket.close()
    debug('The server and client socket connection are closed')
