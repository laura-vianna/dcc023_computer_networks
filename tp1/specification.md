Last Updated on 15/09/17

Universidade Federal de Minas Gerais - DCC023: Redes de Computadores
Trabalho Prático 1 - Data framing

**Introduction**

In this work we will develop an emulator of

data for communication in networks using the fictitious link layer

DCCNET. The software will implement sequencing, framing

and error detection.

**Framework**

The format of a DCCNET frame is shown in diagram 1. The sequence

used for synchronization between the end points (SYNC, 32 bits) is

pre 0xDCC023C2. The error detection field (chksum, 16 bits) uses the

Internet checksum algorithm for error detection. 1 Field

size (length, 16 bits) counts the amount of data transmitted in the

frame; the size field does not count bytes from the frame header. O

rservd field (16 bits) is reserved for future use and will not be used

in this job. The length field must be transmitted using network byte

order (big-endian).

DIAGRAM 1: Format of a DCCNET frame

**Transmitter**

In order to transmit data, the transmitting node creates a frame as

In Figure 1, the checksum and length fields must be initialized.

respectively, with the checksum value of the frame and with the

amount of bytes of data sent in the frame. All Pictures

must have a value greater than zero in the size field.

nho. Frames are transmitted asynchronously and unidirectionally, this

the transmitter does not expect any signal or acknowledgment from the receiver to stream. Note that there is no confirmation or retransmission of frames.

**Receiver**

A data frame can only be accepted if it is valid. The validity

of a table includes verification of the value of the length field (which must

be greater than zero) and the checksum. When accepting a data frame,

the receiver must write the received data (the payload) to a file.

Note that there is no data retransmission in case of an error in reception.

After receiving two occurrences of the synchronization pattern,

the receiver must start receiving a frame. For this the program

shall:

1\. Wait for a number of bytes corresponding to the field value

length. Occurrences of synchronization patterns should occur during re-

These bytes must be ignored.

2\. Calculate the package checksum to detect errors as described

below.

Detection of Errors

Transmission errors are detected using the checksum present in the header

of the package. The checksum is the same as that used on the Internet and is

the entire table, including header and data. Although the rservd field

not be used in this work, it must be considered in the calculation of the

checksum (note that the rservd field can have arbitrary value). During

the checksum calculation, the header bits reserved for the checksum de-

should be considered zero. Error packages should not be

accepted by destiny, being simply ignored.

To recover the frame after detecting an error, your

receiver must wait for two occurrences of the synchronization pattern

(SYNC) that marks the beginning of a frame. The receiver must then try

receive a the frame starting with the markers, checking the checksum

of the entire payload. As described above, the payload is defined by the value

of the field length.

Note that the receiver may be unaware when the frames begin

or terminate, for example, due to (1) transmission error in the field

length of a frame as well as (2) insertion (or removal)

broken (trash) in the channel. Errors of transmission, insertion or removal of bytes may occur during the transmission of a frame or between the

transmission of two frames. For this reason, your receiver should

synchronization patterns (SYNC) to retrieve

the frame after a transmission error.

The receiver must receive correctly and print to the output file

the payloads of all valid frames received.

**Implementation**

You must implement DCCNET over a TCP connection. 2 Your emu-

DCCNET must be able to function as transmitter and receiver.

simultaneously. Your emulator should print to a file, in

hexadecimal, the data in the payloads of all frames received.

Your emulator should read the data to be transmitted from a file.

The contents of the file should be framed and transmitted to the receiver

at the other end of the emulated link.

Your emulator should interoperate with other emulators (test with

emulators), including with the reference emulator imple-

teacher.

**Execution**

Your program should receive five parameters as below (in the same

order):

./emulator <in> <out> <ip> <port> <mode>

Where <in> is the name of the input file to be framed and trans-

mitido; <out> is the name of the file where the valid frame payloads

must be stored; <ip> and <port> indicate an IP address and port; and

<mode> describes the mode of operation of the emulator. The mode of operation

must be entered in full on the command line; there are two modes

operation:

- Active: In the active mode, the program connects to the IP and port

to start transmitting and receiving data.

- passive: In passive mode, the program opens a socket to re-

connections and IP address. The transmission is

2) Use socket (AF INET, SOCK STREAM, 0) to create the socket. This simplifies

development of work data reception takes place after the connection of a client. The service

can terminate after the connection to the first client is

(no more than one client is required).

**Termination**

The emulator may stop executing whenever a connection is

remote node (regardless of whether the emulator was started on the

client mode or server mode). The emulator can also stop

when you receive a keyboard interrupt signal (ctrl-c), this will

is, when it is finished manually. The emulator should not stop

when the file is streamed.

**Evaluation**

This work is in double. The work can be implemented in Python,

C, C ++ or Java, but should interoperate with emulators written in other

languages. Any inconsistency or ambiguity in the specification shall

be pointed at the teacher; if the inconsistency or ambiguity

the student who pointed it will receive two extra points. The student should

deliver PDF documentation of up to 4 pages (two sheets), without cover,

using font size 10, and figures of size appropriate to the size

the source. Documentation should discuss challenges, difficulties and

design visions, as well as the solutions adopted for the problems.

The <out> file generated by your program should only contain pay-

loads correctly. The <out> file should not contain

debug messages, or any other output. All

the data contained in the <out> file will be considered as received

and will be considered in the evaluation of the correctness of their pro-

grass.

**Tests**

At least the following tests will be performed during the evaluation of your

emulator

- Transmission and reception of data in parallel.

- Recovering the frame after transmission errors (we will

programs that intentionally generate wrong bytes in the

saw).

To verify that your program correctly detects synchronization errors,

you can create altered versions of the program that generate intentional-

wrong frames in the send, to see how the program of the other

side behaves. It is not necessary to deliver these used programs

for testing, only the program that works exactly as it is

specification.

**Example**

To transmit four bytes with values ​​1, 2, 3 and 4 (in this order), the

The following bytes must be transmitted on the link layer (assuming

that the rservd field has zero value):

dcc023c2dcc023c2faef0004000001020304

Note that if your emulator should be able to receive correctly

the frame above even after transmission errors that resemble the

beginning of a table. For example, in the example below, your emulator

must be able to correctly receive the valid frame.

/ --- transmission error --- \ / - junk - \ / --- valid frame ----------------

dcc023c2dcc023c2CCCC0001AAAA12345678901dcc023c2dcc023c2faef0004000001020304
